#include "gl_render.h"
#include "consts.h"

void drawRect(sf::Texture texid, float x, float y, float size) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0f, WINDOW_WIDTH, WINDOW_HEIGHT, 0.0f, 0.0f, 1.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	sf::Texture::bind(&texid);
	glColor3f(1.0f, 1.0f, 1.0f);
	glPushMatrix();
	glTranslatef(x, y, 100);
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);
	glVertex2f(x, y);
	glTexCoord2f(1, 0);
	glVertex2f(x + size, y);
	glTexCoord2f(1, 1);
	glVertex2f(x + size, y + size);
	glTexCoord2f(0, 1);
	glVertex2f(x, y + size);
	glEnd();
	glPopMatrix();
	sf::Texture::bind(0);
}

// draws texture in 3d space but facing camera to look 2d
void drawTexture3d(sf::Texture tex, float x, float y, float z, float size) {
/*
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0f, WINDOW_WIDTH, WINDOW_HEIGHT, 0.0f, -4000.0f, 4000.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
*/	
	sf::Texture::bind(&tex);
	glColor3f(1.0f, 1.0f, 1.0f);
	glPushMatrix();
	glTranslatef(x, y, z);

	glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
	glVertex3f(x, y, z);
	glTexCoord2f(1, 0);
	glVertex3f(x + size, y, z - size);
	glTexCoord2f(1, 1);
	glVertex3f(x + size, y + size, z - size);
	glTexCoord2f(0, 1);
	glVertex3f(x, y + size, z);
	glEnd();

	glPopMatrix();
	sf::Texture::bind(0);
}

void drawCube(sf::Texture topId, sf::Texture sidesId, float x, float y, float z, float size) {
	glPushMatrix();
	glTranslatef(x, y, z);
	glColor3f(1.0f, 1.0f, 1.0f);

	sf::Texture::bind(&sidesId);
	glBegin(GL_QUADS);
	glVertex3f(x, y, z);
	glVertex3f(x + size, y, z);
	glVertex3f(x + size, y + size, z);
	glVertex3f(x, y + size, z);
	
	glVertex3f(x, y, z + size);
	glVertex3f(x + size, y, z + size);
	glVertex3f(x + size, y + size, z + size);
	glVertex3f(x, y + size, z + size);
		
	glVertex3f(x + size, y, z);
	glVertex3f(x + size, y, z + size);
	glVertex3f(x + size, y + size, z + size);
	glVertex3f(x + size, y + size, z);

	glVertex3f(x, y, z);
	glVertex3f(x, y, z + size);
	glVertex3f(x, y + size, z + size);
	glVertex3f(x, y + size, z);

	glVertex3f(x, y + size, z);
	glVertex3f(x + size, y + size, z);
	glVertex3f(x + size, y + size, z + size);
	glVertex3f(x, y + size, z + size);
	glEnd();
	sf::Texture::bind(0);

	// top
	sf::Texture::bind(&topId);
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);
	glVertex3f(x, y, z);
	glTexCoord2f(1, 0);
	glVertex3f(x + size, y, z);
	glTexCoord2f(1, 1);
	glVertex3f(x + size, y, z + size);
	glTexCoord2f(0, 1);
	glVertex3f(x, y, z + size);
	glEnd();
	sf::Texture::bind(0);
	glPopMatrix();
}
