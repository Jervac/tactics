#ifndef GL_RENDER_H
#define GL_RENDER_H

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

void drawRect(float x, float y, float size);
void drawTexture3d(sf::Texture textureId, float x, float y, float z, float size);
void drawCube(sf::Texture top, sf::Texture sides, float x, float y, float z, float size);

#endif
