#include "game.h"
/*
float rot = 0.8f;
float m_width = 1280;
float m_height = 720;

const int WIDTH = 1280;
const int HEIGHT = 720;

void drawRect(float x, float y, float size);
void drawTexture3d(float x, float y, float z, float size);
void drawCube(float x, float y, float z, float size);
void drawRect3d(float x, float y, float z, float size);

sf::Texture texLid;
sf::Texture dirtId;
std::string image2="tile.png";
std::string dirt="dirt.png";
sf::Texture characterId;
std::string character="character.png";
*/
int main() {
	Game g;
	g.init();
	g.loop();
	return 0;
	
/*
	if (!texLid.loadFromFile(image2)) {
		std::cout << "Could not load" << image2;
		char c;
		std::cin>>c;
		return false;
	}

	if (!dirtId.loadFromFile(dirt)) {
		std::cout << "Could not load" << dirt;
		char c;
		std::cin>>c;
		return false;
	}

	if (!characterId.loadFromFile(character)) {
		std::cout << "Could not load" << character;
		char c;
		std::cin>>c;
		return false;
	}
    
    sf::Window window(sf::VideoMode(WIDTH, HEIGHT),
					  "OpenGL",
					  sf::Style::Titlebar,
					  sf::ContextSettings(21));
    window.setVerticalSyncEnabled(true);
    window.setActive(true);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0f, 1280, 720, 0.0f, -4000.0f, 4000.0f);
	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);


    bool running = true;
    while (running) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                running = false;
            } else if (event.type == sf::Event::Resized) {
                glViewport(0, 0, event.size.width, event.size.height);
            }
        }

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(.1f, .1f, .1f, 1);
		glEnable (GL_BLEND);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		

		

	    float rott = 52;
		glRotatef(rott, -.5f, -1.0f, 0.25f);
		for(int i=0; i<8; i++) {
			for(int j=0; j<10; j++) {
				drawCube(500 + (i * 32), -200, 200 + (j * 32), 64);
			}
		}
		glRotatef(-rott, -.5f, -1.0f, 0.25f);


		glDisable (GL_BLEND);
        window.display();
    }

	return 0;
*/
}

/*
void drawRect(float x, float y, float size) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0f, WIDTH, HEIGHT, 0.0f, 0.0f, 1.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	sf::Texture::bind(&texLid);
	glColor3f(1.0f, 1.0f, 1.0f);
	glPushMatrix();
	glTranslatef(x, y, 100);
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);
	glVertex2f(x, y);
	glTexCoord2f(1, 0);
	glVertex2f(x + size, y);
	glTexCoord2f(1, 1);
	glVertex2f(x + size, y + size);
	glTexCoord2f(0, 1);
	glVertex2f(x, y + size);
	glEnd();
	glPopMatrix();
	sf::Texture::bind(0);
}

void drawRect3d(float x, float y, float z, float size) {
	glColor3f(1.0f, 1.0f, 1.0f);
	glPushMatrix();
	glTranslatef(x, y, z);

	glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
	glVertex3f(x, y, z);
	glTexCoord2f(1, 0);
	glVertex3f(x + size, y, z);
	glTexCoord2f(1, 1);
	glVertex3f(x + size, y + size, z);
	glTexCoord2f(0, 1);
	glVertex3f(x, y + size, z);
	glEnd();

	glPopMatrix();
}

// draws texture in 3d space but facing camera to look 2d
void drawTexture3d(float x, float y, float z, float size) {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0f, WIDTH, HEIGHT, 0.0f, -4000.0f, 4000.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	sf::Texture::bind(&characterId);
	glColor3f(1.0f, 1.0f, 1.0f);
	glPushMatrix();
	glTranslatef(x, y, z);

	glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
	glVertex3f(x, y, z);
	glTexCoord2f(1, 0);
	glVertex3f(x + size, y, z - size);
	glTexCoord2f(1, 1);
	glVertex3f(x + size, y + size, z - size);
	glTexCoord2f(0, 1);
	glVertex3f(x, y + size, z);
	glEnd();

	glPopMatrix();
	sf::Texture::bind(0);
}

void drawCube(float x, float y, float z, float size) {
	glPushMatrix();
	glTranslatef(x, y, z);
	glColor3f(1.0f, 1.0f, 1.0f);

	sf::Texture::bind(&dirtId);
	glBegin(GL_QUADS);
	glVertex3f(x, y, z);
	glVertex3f(x + size, y, z);
	glVertex3f(x + size, y + size, z);
	glVertex3f(x, y + size, z);
	
	glVertex3f(x, y, z + size);
	glVertex3f(x + size, y, z + size);
	glVertex3f(x + size, y + size, z + size);
	glVertex3f(x, y + size, z + size);
		
	glVertex3f(x + size, y, z);
	glVertex3f(x + size, y, z + size);
	glVertex3f(x + size, y + size, z + size);
	glVertex3f(x + size, y + size, z);

	glVertex3f(x, y, z);
	glVertex3f(x, y, z + size);
	glVertex3f(x, y + size, z + size);
	glVertex3f(x, y + size, z);

	glVertex3f(x, y + size, z);
	glVertex3f(x + size, y + size, z);
	glVertex3f(x + size, y + size, z + size);
	glVertex3f(x, y + size, z + size);
	glEnd();
	sf::Texture::bind(0);

	// top
	sf::Texture::bind(&texLid);
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0);
	glVertex3f(x, y, z);
	glTexCoord2f(1, 0);
	glVertex3f(x + size, y, z);
	glTexCoord2f(1, 1);
	glVertex3f(x + size, y, z + size);
	glTexCoord2f(0, 1);
	glVertex3f(x, y, z + size);
	sf::Texture::bind(0);		
	glEnd();
	glPopMatrix();
}
*/
