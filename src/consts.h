#ifndef CONSTS_H
#define CONSTS_H

#include <string>

const std::string WINDOW_TITLE = "Game window";
const int WINDOW_WIDTH = 1280;
const int WINDOW_HEIGHT = 720;
const int TILESIZE = 64;
const int MAP_WIDTH = 10;
const int MAP_HEIGHT = 10;
const int MAX_PLAYABLE_CHARACTERS = 3;
const int MAX_AI_CHARACTERS = 3;
#endif
