#include "aicharacter.h"
#include "../game.h"
#include "math.h"
#include "../utils.h"

AICharacter::AICharacter(Game* ngame, float x, float y) : Character(ngame, x, y) {
	playable = false;
	texture.loadFromFile("aicharacter.png");
	sprite.setTexture(texture);
}

void AICharacter::update() {
	if(stats.health <= 0) alive = false;
	// change target_tile to random tile during it's turn
	if(getHighlighted() && target_tile == NULL && !had_turn) {
		moveToRandomTile();
	}


	// go to target_tile
	if(getHighlighted() && target_tile != NULL) {
		float dx = target_tile->position.x - position.x;
		float dy = target_tile->position.y - position.y;
		float len = sqrt((dx*dx)+(dy*dy));
		dx /= len;
		dy /= len;
		position.x += floor(dx * vel);
		position.y += floor(dy * vel);
	}

	// end turn when sprite reached the target tile
	if(target_tile == getCurrentTile()) {

		// allign sprite neatly on tile to prevent rounding problems
		position.x = target_tile->position.x;
		position.y = target_tile->position.y;
		target_tile = NULL;
		setHighlighted(false);
		game->cycleCharacters();
	}
}

void AICharacter::render() {
//	sprite.setPosition(position);
//	window.draw(sprite);
}

void AICharacter::moveToRandomTile() {
	std::srand(static_cast<int>(time(0)));
	while(true) {
		int randIndex = rand()%(game->tiles.size()-1)+0;
		if(!game->tiles[randIndex]->containsCharacter()) {
			target_tile = game->tiles[randIndex];
			break;
		}
	}
}
