#include "character.h"
#include "../game.h"
#include "math.h"
#include "../utils.h"

Character::Character(Game* ngame, float x, float y) {
	game = ngame;
	position = sf::Vector2f(x, y);
	position.x = x;
	position.y = y;
	size.x = TILESIZE;
	size.y = TILESIZE;
	
	texture.loadFromFile("character.png");
	sprite.setTexture(texture);
	sprite.setPosition(position.x, position.y);
	sprite.setScale(size.x / sprite.getGlobalBounds().width, size.y / sprite.getGlobalBounds().height);

	initRandomStats();
}

// TODO: This isn't ever called with current character deletion!
Character::~Character() {
	std::cout << "destroying dead character" << std::endl;
}

// TODO: Make this actually random
void Character::initRandomStats() {
	stats.health = 1;
	stats.attack = 3;
	stats.defence = 3;
	stats.speed = 2;
}

void Character::moveToPosition(sf::Vector2f target) {
	position.x = target.x;
	position.y = target.y;
}

void Character::move(Tile* t) {
	target_tile = t;
}

Tile* Character::getCurrentTile() {
	for(auto t : game->tiles) {
		if(floor(t->position.x / TILESIZE) == floor(position.x / TILESIZE) &&
		   floor(t->position.y / TILESIZE) == floor(position.y / TILESIZE)) {
			return t;
		}
	}
}

void Character::setHighlighted(bool b) {
	highlighted = b;
	if(b) {
		sprite.setColor(sf::Color::Yellow);
	} else {
		if(had_turn) {
			sprite.setColor(sf::Color::Black);
		} else {
			sprite.setColor(sf::Color::White);
		}
	}
}

void Character::hurt(int damage) {
	stats.health -= damage;
}

void Character::attack(std::string attack, Character* target) {
	// TODO: have attackamanger handle parsing
	// attack_manager will also be able to highlight tiles you can select
	if(attack == attack_manager.WARRIOR_SWORD_SLASH) {
		if(attack_manager.isTargetHorizontalVertical(game, this, target, 1)) {
			println("attacked an enemy");
			target->hurt(1);
			performed_action = true;
		}
	}

	action = IDLE;
}

bool Character::getHighlighted() {
	return highlighted;
}

bool Character::isMoving() {
	return (target_tile != NULL);
}
