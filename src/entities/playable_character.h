#ifndef PLAYABLE_CHARACTER_H
#define PLAYABLE_CHARACTER_H

#include "character.h"

struct PlayableCharacter : public Character {
	PlayableCharacter(Game* ngame, float x, float y);
	void update() ;
	void render();
};
#endif
