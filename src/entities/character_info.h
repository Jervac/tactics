#ifndef CHARACTER_INFO_H
#define CHARACTER_INFO_H

////////////////////
// Action State
////////////////////
enum Action_State {
	IDLE,
	MOVE,
	ATTACK,
	DONE,
};

////////////////////
// Stats
////////////////////
struct Stat_Info {
	int health;
	int attack;
	int defence;
	int speed;
};

////////////////////
// Classes
////////////////////
enum Class_Type {
	WARRIOR,
	ARCHER,
	MAGE,
};

#endif
