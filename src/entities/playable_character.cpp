#include "playable_character.h"
#include "../utils.h"
#include "math.h"
#include "../game.h"
#include "../gl_render.h"
#include <SFML/Graphics.hpp>
bool pressed = false;
PlayableCharacter::PlayableCharacter(Game *ngame, float x, float y) 
	: Character(ngame, x, y) {
}

void PlayableCharacter::update() {
	if(stats.health <= 0) alive = false;

	// logic when highlighted. set action to IDLE at end of an action being performed
	if(getHighlighted()) {

		// IDLE
		if(action == IDLE) {
		
		}

		// Movement
		if(action == MOVE && !performed_move) {

			// go to target_tile
			if(target_tile != NULL) {
				float dx = target_tile->position.x - position.x;
				float dy = target_tile->position.y - position.y;
				float len = sqrt((dx*dx)+(dy*dy));
				dx /= len;
				dy /= len;
				position.x += floor(dx * vel);
				position.y += floor(dy * vel);
			}

			// when sprite reaches destination
			if(target_tile == getCurrentTile()) {

				// allign sprite neatly on tile to prevent rounding problems
				position.x = target_tile->position.x;
				position.y = target_tile->position.y;
				target_tile = NULL;
				action = IDLE;
				performed_move = true;
			}
		}
	
		// ATTACK
		if(action == ATTACK && !performed_action) {
		//	action = IDLE;
		//	performed_action = true;
		}
	

		// DONE
		if(action == DONE) {
			action = IDLE;
			performed_action = false;
			performed_move   = false;
			setHighlighted(false);
			game->cycleCharacters();
		}
	}

}

void PlayableCharacter::render() {	
	drawTexture3d(texture, 500 + (TILESIZE * (position.x/TILESIZE)) - 0, -200 - size.x/2, 200 + (TILESIZE * (position.y/TILESIZE)) + size.y/2, size.x);
}
