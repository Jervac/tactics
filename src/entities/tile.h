#ifndef TILE_H
#define TILE_H

#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>
#include "../consts.h"
#include "character.h"

struct Game;

struct Tile {
	Game* game;

	sf::Vector2f position;
	sf::Vector2f size;

	bool highlight = false;

	sf::Texture texture;
	sf::Texture dirt;

	Tile(Game* ngame, float x, float y);

	void update();
	
	void render();

	sf::Vector2f toTileCoords(sf::Vector2f position);

	sf::Vector2f getTileCoords();

	// returns true if there is a character with the same tile position as this tile
	bool containsCharacter();

	Character* getCurrentCharacter();
};
#endif
