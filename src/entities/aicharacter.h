#ifndef AICHARACTER_H
#define AICHARACTER_H

#include "character.h"

struct AICharacter : public Character {
	AICharacter(Game* ngame, float x, float y);

	void update();
	void render();

	// move to a random tile
	void moveToRandomTile();
};
#endif
