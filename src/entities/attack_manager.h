#ifndef ATTACK_MANAGER_H
#define ATTACK_MANAGER_H

#include <string>
#include <vector>

using std::string;

struct Game;
struct Character;
struct Tile;

struct AttackManager {
	const string WARRIOR_SWORD_SLASH = "warrior_sword_slash";
	// parses attack and checks if that character can even use the specified move
//	bool parseAttack(string attack, Charcter* character);
	
	// Checks up down left right in a range for the target character
	bool isTargetHorizontalVertical(Game* game, Character* character, Character* target, int range);
};
#endif
