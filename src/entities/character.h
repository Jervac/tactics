#ifndef CHARACTER_H
#define CHARACTER_H

#include <SFML/Graphics.hpp>
#include <vector>
#include "../consts.h"
#include "character_info.h"
#include "attack_manager.h"

struct Game;
struct Tile;
//TODO: add Class_Type and other structs here

struct Character {
	Character(Game* ngame, float x, float y);
	~Character();
	
	Class_Type class_type = WARRIOR;
	Stat_Info stats;
	Action_State action = IDLE;
	AttackManager attack_manager;

	bool alive = true;
	// bools prevent you from acting / moving twice
	bool performed_action = false;
	bool performed_move = false;

	Game* game;

	// always set to NULL unless character needs to move to target
	Tile* target_tile = NULL;

	sf::Vector2f position = sf::Vector2f(0,0);
	sf::Vector2f size = sf::Vector2f(0,0);

	sf::Sprite sprite;
	sf::Texture texture;

	bool playable = true;
	bool had_turn = false;
	
	// velocity sprite moves towards position at
	float vel = 4;

	virtual void update() = 0;
	virtual void render() = 0;
	
	void initRandomStats();

	void moveToPosition(sf::Vector2f target);

	void move(Tile* tile);
	
	void hurt(int damage);

	// moves sprite along vector towards position. call once every render
	void glideSprite();

	// colors sprite yellow if true and black if false and had_turn
	void setHighlighted(bool h);

	void attack(std::string attack, Character* target);

	bool getHighlighted();

	// returns true if target_tile is != NULL meaning there's a target being moved towards
	bool isMoving();

	// get tile character is standing on
	Tile* getCurrentTile();	

private:
	bool highlighted = false;
};
#endif
