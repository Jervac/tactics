#include "attack_manager.h"
#include "tile.h"
#include "../game.h"
#include "../utils.h"

bool AttackManager::isTargetHorizontalVertical(Game* game, Character* character, Character* target, int range) {
	for(auto t : game->tiles) {

		// iteration starts at 1 to save trouble of checking if enemy is on top of you
		if(game->toTileCoords(target->position).x == game->toTileCoords(character->position).x) {

			// up
			for(int i=1; i<range+1; i++) {
				if(game->toTileCoords(target->position).y == game->toTileCoords(character->position).y-i) {
					return true;
				}
			}
			
			// down
			for(int i=1; i<range+1; i++) {
				if(game->toTileCoords(target->position).y == game->toTileCoords(character->position).y+i) {
					return true;
				}
			}
		} else if(game->toTileCoords(target->position).y == game->toTileCoords(character->position).y) {

			// left
			for(int i=1; i<range+1; i++) {
				if(game->toTileCoords(target->position).x == game->toTileCoords(character->position).x-i) {
					return true;
				}
			}

			// right
			for(int i=1; i<range+1; i++) {
				if(game->toTileCoords(target->position).x == game->toTileCoords(character->position).x+i) {
					return true;
				}
			}
		}
		
	}
	return false;
}
