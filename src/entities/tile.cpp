#include <cmath>
#include "tile.h"
#include "../game.h"
#include "../gl_render.h"

Tile::Tile(Game* ngame, float x, float y) {
	position.x = x;
	position.y = y;
	size.x = TILESIZE;
	size.y = TILESIZE;
	game = ngame;

	texture.loadFromFile("tile.png");
	dirt.loadFromFile("dirt.png");
}

void Tile::update() {}

void Tile::render() {
/*
	sf::RectangleShape shape;
	shape.setSize(size);
	shape.setPosition(position);
*/

	if(highlight) {
		drawCube(dirt, texture, 500 + (getTileCoords().x * (TILESIZE/2)), -200, 200 + (getTileCoords().y * (TILESIZE/2)), TILESIZE);
	
	} else {
		drawCube(texture, dirt, 500 + (getTileCoords().x * (TILESIZE/2)), -200, 200 + (getTileCoords().y * (TILESIZE/2)), TILESIZE);
	}
//	window.draw(sprite);


/*
	shape.setOutlineColor(sf::Color::Black);
	shape.setOutlineThickness(2);
	window.draw(shape);
*/
	highlight = false;
}

sf::Vector2f Tile::toTileCoords(sf::Vector2f pos) {
	return sf::Vector2f(pos.x / TILESIZE, pos.y / TILESIZE);
}

sf::Vector2f Tile::getTileCoords() {
	return sf::Vector2f(floor(position.x / TILESIZE), floor(position.y / TILESIZE));
}

bool Tile::containsCharacter() {
	for(auto c : game->characters) {
		if(toTileCoords(c->position).x == toTileCoords(position).x &&
		   toTileCoords(c->position).y == toTileCoords(position).y) {
			return true;
		}
	}
	return false;
}

Character* Tile::getCurrentCharacter() {
	for(auto c : game->characters) {
		if(toTileCoords(c->position).x == toTileCoords(position).x &&
		   toTileCoords(c->position).y == toTileCoords(position).y) {
			return c;
		}
	}
	return NULL;
}
