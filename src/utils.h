#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <string>

////////////////////////////////////////
// Utilities to simplify code verbosity
////////////////////////////////////////

template <typename T>
inline void println(T const& t) {
	std::cout << t<< std::endl;
};

#endif
