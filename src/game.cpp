#include "game.h"
#include <SFML/Window/Event.hpp>
#include <SFML/OpenGL.hpp>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include "utils.h"
#include "entities/character_info.h"
#include "entities/aicharacter.h"
#include "gl_render.h"
#include "consts.h"

sf::Texture texLid;
sf::Texture dirtId;
std::string image2="tile.png";
std::string dirt="dirt.png";
sf::Texture characterId;
std::string character="character.png";


void Game::init() {
/*
    camera = sf::View(sf::FloatRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT));
    window.create(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), WINDOW_TITLE, sf::Style::Titlebar);
	sf::Vector2i window_pos(sf::VideoMode::getDesktopMode().width/2 - (window.getSize().x/2),
							sf::VideoMode::getDesktopMode().height/2 - (window.getSize().y/2));
	window.setPosition(window_pos);
	window.setFramerateLimit(60);
    camera.zoom(1);
    window.setView(camera);
	
*/
    for(int i=0; i<MAP_WIDTH; i++) {
		for(int j=0; j<MAP_HEIGHT; j++) {
			auto* t1 = new Tile(this, i * TILESIZE, j * TILESIZE);
			tiles.push_back(t1);
		}
	}
	
	
    // load textures
	if (!texLid.loadFromFile(image2)) {
		std::cout << "Could not load" << image2;
		char c;
		std::cin>>c;
	}

	if (!dirtId.loadFromFile(dirt)) {
		std::cout << "Could not load" << dirt;
		char c;
		std::cin>>c;
	}
	if (!characterId.loadFromFile(character)) {
		std::cout << "Could not load" << character;
		char c;
		std::cin>>c;
	}
    

	// init window / opengl
	window.create(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT),
					  "OpenGL",
					  sf::Style::Titlebar,
					  sf::ContextSettings(21));
    window.setVerticalSyncEnabled(true);
    window.setActive(true);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0f, WINDOW_WIDTH, WINDOW_HEIGHT, 0.0f, -4000.0f, 4000.0f);
	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);

	auto* c = new PlayableCharacter(this, 32 * 0, 32 * 0);
	characters.push_back(c);
}

void Game::update() {

    for(auto t : tiles) {
		t->update();
	}

	for(auto c : characters) {
		c->update();
	}
/*
	// activate action state when there are characters moving
	if(state.getCurrent() != state.ACTION) {
		for(auto c : characters) {
			if(c->isMoving()) {
				state.setCurrent(state.ACTION);
				break;
			}
		}
	}
*/
}

void Game::render() {
/*
   	window.clear(sf::Color(19,19,19,255));

    for(auto t : tiles) {
		t->render(window);
	}

	for(int i=0; i<characters.size(); i++) {
		if(characters[i]->alive) {
			characters[i]->update();
		} else {
			characters.erase(std::remove(characters.begin(), characters.end(), characters[i]), characters.end());
		}
	}

	for(auto c : characters) {
		if(c->alive) {
			c->render(window);
		} else {
			delete c;
		}
	}
*/

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(.1f, .1f, .1f, 1);
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	float rott = 52;
	glRotatef(rott, -.5f, -1.0f, 0.25f);
    for(auto t : tiles) {
		if(cursor_position.x == t->getTileCoords().x && cursor_position.y == t->getTileCoords().y) {
			t->highlight = true;
		}
		t->render();
	}
	for(auto c : characters) {
		c->render();
	}
//	float spriteSize = 50;
//	int x = 4, y = 6;
//	drawTexture3d(characterId, 500 + (32 * x) - 0, -200 - spriteSize/2, 200 + (32 * y) + spriteSize/2, spriteSize);
//	drawTexture3d(characterId, 500 + (32 * (x - 2)) - 0, -200 - spriteSize/2, 200 + (32 * (y-1)) + spriteSize/2, spriteSize);	
	glRotatef(-rott, -.5f, -1.0f, 0.25f);

	glDisable (GL_BLEND);
	window.display();
}

void Game::input() {

	// cursor control
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
		if(!moved_cursor) {
			moved_cursor = true;
			if(cursor_position.x < MAP_WIDTH) {
				cursor_position.x++;
			}
		}
	} else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
		if(!moved_cursor) {
			moved_cursor = true;
			if(cursor_position.x > 0) {
				cursor_position.x--;
			}
		}
	} else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
		if(!moved_cursor) {
			moved_cursor = true;
			if(cursor_position.y > 0) {
				cursor_position.y--;
			}
		}
	} else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
		if(!moved_cursor) {
			moved_cursor = true;
			if(cursor_position.y < MAP_HEIGHT) {
				cursor_position.y++;
			}
		}
	} else {
		moved_cursor = false;
	}

	// escape to exit game
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
		window.close();
	}

	// toggle debug mode
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::F1)) {
		debug = !debug;
	}

	// highlight tile being hovered on
	for(auto t : tiles) {
		if(mouseWorldCoords().x > t->position.x &&
		   mouseWorldCoords().x < t->position.x + t->size.x &&
		   mouseWorldCoords().y > t->position.y &&
		   mouseWorldCoords().y < t->position.y + t->size.y) {
			t->highlight = true;
		}
	}		

	// mouse-based camera movement
	if(sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
		sf::Vector2f windowCenter = sf::Vector2f(window.getSize().x/2,
												 window.getSize().y/2);
		sf::Vector2f mousePos;
		mousePos.x  = sf::Mouse::getPosition(window).x;
		mousePos.y  = sf::Mouse::getPosition(window).y;

		// calculate normalized vector for direction camera needs to go towards
		float dx = (mousePos.x - windowCenter.x);
		float dy = (mousePos.y - windowCenter.y);
		float len = static_cast<float>(sqrt((dx * dx) + (dy * dy)));

		if(len != 0) {
			dx /= len;
			dy /= len;
			mouseDir.x = dx;
			mouseDir.y = dy;
		}
		
	//	window.setView(camera); uncomment me!
		camera.setCenter(camera.getCenter().x + (mouseDir.x * cameraVel),
						 camera.getCenter().y + (mouseDir.y * cameraVel));
	}
	

	// add your characters to the map
	if(state.getCurrent() == state.SELECT) {

		// if player has placed the max amount of characters, start fight
		if(playableCharactersCount() == MAX_PLAYABLE_CHARACTERS) {
			if(debug) std::cout << "Battle Started!" << std::endl;
			spawnEnemyCharacters();
			getNextCharacter()->setHighlighted(true);
			state.setCurrent(state.IDLE);
		} else if(sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
			if(!pressedMouse) {
				pressedMouse = true;

				// places character if possible
				for(auto t : tiles) {
					if(floor(t->position.x / TILESIZE) == floor(mouseWorldCoords().x / TILESIZE) &&
					   floor(t->position.y / TILESIZE) == floor(mouseWorldCoords().y / TILESIZE)) {
						if(playableCharactersCount() < MAX_PLAYABLE_CHARACTERS &&
						   !t->containsCharacter()) {
							auto* c = new PlayableCharacter(this, t->position.x, t->position.y);
							characters.push_back(c);
							if(debug) std::cout << "added character" << std::endl;
						} else {
							if(debug) std::cout << "can't place a character here!" << std::endl;
						}
					}
				}
			}
		} else {
			pressedMouse = false;
		}
	}

	// move your character on the map to a new position
	if(state.getCurrent() == state.IDLE) {
		if(sf::Mouse::isButtonPressed(sf::Mouse::Left) && getCurrentCharacter()->action == MOVE) {
			if(!pressedMouse) {
				pressedMouse = true;

				// move current character to tile at mouse position
				if(getMousedTile() != NULL && getCurrentCharacter()->playable) {
					if(!getMousedTile()->containsCharacter()) {
						getCurrentCharacter()->move(getMousedTile());
						if(debug) std::cout << "you just moved your character" << std::endl;
					}
				}
			}
		} else {
			pressedMouse = false;
		}
	}
	
	
	// character keyboard options
	if(getCurrentCharacter() != NULL &&
	   getCurrentCharacter()->playable &&
	   getCurrentCharacter()->action == IDLE) {
		
		// character acted		
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::X)) {
			if(!pressed_action) {
				pressed_action = true;
				if(!getCurrentCharacter()->performed_action) {		
					getCurrentCharacter()->action = ATTACK;
				}
			}
			
		} // character moved
		else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Z)) {
			if(!pressed_action) {
				pressed_action = true;
				if(!getCurrentCharacter()->performed_move) {
					getCurrentCharacter()->action = MOVE;
				}
			}
			
		} // end turn
		else if(sf::Keyboard::isKeyPressed(sf::Keyboard::C)) {
			if(!pressed_action) {
				pressed_action = true;
				getCurrentCharacter()->action = DONE;
			}
		} else {
			pressed_action = false;
		}
	}

	// character attack
	if(getCurrentCharacter() != NULL && getCurrentCharacter()->action == ATTACK &&
	   !getCurrentCharacter()->performed_action) {
		
		if(sf::Mouse::isButtonPressed(sf::Mouse::Left) &&
		   getMousedTile() != NULL &&
		   getMousedTile()->getCurrentCharacter() != NULL) {
		
			if(!getMousedTile()->getCurrentCharacter()->playable) {
				getCurrentCharacter()->attack("warrior_sword_slash", getMousedTile()->getCurrentCharacter());
			}
		}
	}	
}
		
		
int Game::playableCharactersCount() {
	int count = 0;
	for(auto c : characters) {
		if(c->playable) {
			count++;
		}
	}
	return count;
}

int Game::aiCharactersCount() {
	int count = 0;
	for(auto c : characters) {
		if(!c->playable) {
			count++;
		}
	}
	return count;
}

sf::Vector2f Game::toTileCoords(sf::Vector2f position) {
	return sf::Vector2f(position.x / TILESIZE, position.y / TILESIZE);
}

sf::Vector2f Game::mouseWorldCoords() {
	sf::Vector2i mouse = sf::Mouse::getPosition(window);
//	return window.mapPixelToCoords(mouse); uncomment me!
	return sf::Vector2f(0,0);
}

void Game::spawnEnemyCharacters() {
	std::srand(static_cast<int>(time(0)));
	for(int i=0; i<MAX_AI_CHARACTERS; i++) {
		while(true) {
			int randIndex = rand()%(tiles.size()-1)+0;
			if(!tiles[randIndex]->containsCharacter()) {
				auto* c = new AICharacter(this, tiles[randIndex]->position.x, tiles[randIndex]->position.y);
				characters.push_back(c);
				break;
			}
		}
	}
}

bool Game::allTurnsTaken() {
	for(auto c : characters) {
		if(!c->had_turn) {
			return false;
		}
	}
	return true;
}

Character* Game::getCurrentCharacter() {
	for(auto c : characters) {
		if(c->getHighlighted()) {
			return c;
		}
	}
	return NULL;
}

Character* Game::getNextCharacter() {
	int fastest_speed = 0;
	for(auto c : characters) {
		c->setHighlighted(false);
		if(c->stats.speed > fastest_speed && !c->had_turn) {
			fastest_speed = c->stats.speed;
		}
	}
	for(auto c : characters) {
		if(c->stats.speed == fastest_speed && !c->had_turn) {
			return c;
		}
	}
	return NULL;
}

Tile* Game::getMousedTile() {
	for(auto t : tiles) {
		if(t->getTileCoords().x == floor(mouseWorldCoords().x / TILESIZE) &&
		   t->getTileCoords().y == floor(mouseWorldCoords().y / TILESIZE)) {
			return t;
		}
	}
	return NULL;
}

void Game::cycleCharacters() {
	Character* next_character = getNextCharacter();
	Character* current_character = getCurrentCharacter();

	if(next_character != NULL || current_character != NULL) {
		if(next_character != NULL) {
			next_character->had_turn = true;
			
			// if next character is playable, go to IDLE battlestate so it can be given an action
			// also set character action to IDLE
			if(next_character->playable) {
				state.setCurrent(state.IDLE);
				next_character->action = IDLE;
			}
		}

		Character* c2 = getNextCharacter();
		
		// if NULL then all characters have been cycled through so restart
		if(c2 == NULL) {
			if(debug) std::cout << "all characters have had their turn!\nabling them all going at it again!" << std::endl;
			for(auto c : characters) {
				c->had_turn = false;
				getNextCharacter()->setHighlighted(true);
			}
			state.setCurrent(state.IDLE);
			
			// move on to next character
		} else if(c2 != NULL) {
			c2->setHighlighted(true);
		}
	}
}

// TODO: see if dis works tho

void Game::clearCharacters() {
	for(auto c : characters) {
		delete c;
	}
}

void Game::loop() {
    while(window.isOpen()) {
        sf::Event event;
        while(window.pollEvent(event)) {
            if(event.type == sf::Event::Closed) {
                window.close();
            } else if (event.type == sf::Event::Resized) {
                glViewport(0, 0, event.size.width, event.size.height);
            }
        }
        input();
        update();
        render();
//        window.display();
    }

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_TEXTURE_2D);
}

////////////////////
// BattleState
////////////////////

std::string Game::BattleState::getCurrent() {
	return current;
}

void Game::BattleState::setCurrent(std::string ncurrent) {
	current = ncurrent;
}
