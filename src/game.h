#ifndef GAME_H
#define GAME_H

#include <SFML/Graphics.hpp>
#include "consts.h"
#include "entities/tile.h"
#include "entities/character.h"
#include "entities/playable_character.h"

struct Tile;
struct Character;

struct Game {
    sf::Window window;
    sf::View camera;

    float cameraVel = 6;
    sf::Vector2f initialMousePos;
    sf::Vector2f mouseDir = sf::Vector2f(1, 1);
	sf::Vector2f cursor_position = sf::Vector2f(0, 1);
	bool moved_cursor = false; // moved cursor by a tile
    bool pressedMouse = false;
	bool pressed_space = false;
	bool pressed_action = false; // pressed Z, X, or C
    std::vector<Tile*> tiles;
	std::vector<Character*> characters;

	bool debug = false;

    void init();
    void loop();
    void update();
    void render();
    void input();

	void spawnEnemyCharacters();

	void clearCharacters();

    struct BattleState {
        const std::string SELECT = "SELECT"; // select characters and place on map
        const std::string IDLE   = "IDLE";   // characters awaiting player action
		const std::string ACTION = "ACTION"; // characters are performing animated actions
        std::string getCurrent();
        void setCurrent(std::string ncurrent);
	private:
    	std::string current = SELECT;
    } state;
	
    sf::Vector2f mouseWorldCoords();

    sf::Vector2f toTileCoords(sf::Vector2f position);

	int playableCharactersCount();
	
	int aiCharactersCount();

	bool allTurnsTaken();

	// returns currently highlighted character
	Character* getCurrentCharacter();

	// places ai characters randomly on tiles that don't have a character already on them
	Character* getNextCharacter();

	// returns tile mouse is currently touching
	Tile* getMousedTile();

	// highlights the next character
	void cycleCharacters();
};
#endif
