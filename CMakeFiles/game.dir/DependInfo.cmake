# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/you/Projects/tactics/src/entities/aicharacter.cpp" "/home/you/Projects/tactics/CMakeFiles/game.dir/src/entities/aicharacter.cpp.o"
  "/home/you/Projects/tactics/src/entities/attack_manager.cpp" "/home/you/Projects/tactics/CMakeFiles/game.dir/src/entities/attack_manager.cpp.o"
  "/home/you/Projects/tactics/src/entities/character.cpp" "/home/you/Projects/tactics/CMakeFiles/game.dir/src/entities/character.cpp.o"
  "/home/you/Projects/tactics/src/entities/playable_character.cpp" "/home/you/Projects/tactics/CMakeFiles/game.dir/src/entities/playable_character.cpp.o"
  "/home/you/Projects/tactics/src/entities/tile.cpp" "/home/you/Projects/tactics/CMakeFiles/game.dir/src/entities/tile.cpp.o"
  "/home/you/Projects/tactics/src/game.cpp" "/home/you/Projects/tactics/CMakeFiles/game.dir/src/game.cpp.o"
  "/home/you/Projects/tactics/src/gl_render.cpp" "/home/you/Projects/tactics/CMakeFiles/game.dir/src/gl_render.cpp.o"
  "/home/you/Projects/tactics/src/main.cpp" "/home/you/Projects/tactics/CMakeFiles/game.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
