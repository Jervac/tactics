Coding Conventions
----------------------------------

# Files
- Comments on what a function does only in header file
- Comments on what parts of a function do can still be in the .cpp declaration
- File names are underscored and lower case (ex. character_info.h, character_sprite.png)
- Keep folder count to a minimum

# Classes / Structs treated like classes
- Functions are cammelcase starting with lower case (ex. getCurrent())
- Class / Struct name is Camel case starting with captial case (ex. Character)

# Enums / Structs not treated like classes
- Camel case starting with capital and underscored (ex. Character_Stats)


NOTE: MAY NEED REVISION!

# if statements
- Generally always have brackets
- If short enough allow (if(yes) doThings();)
- If paramaters of statement > 2, new line after &&/||
- If the statement uses new lines because of large amount of parameters, add blank line under

// Wrong
if(thingStuffStuff && stuffStuffStuff && thisStuffStuff && thatStuffStuff) {
    if(those && this) {

    }
}

// Right
if(thingStuffStuff() &&
   thatStuffStuff() &&
   thisStuffStuff()) {

   if(thos && this) {

   }
}
